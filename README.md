# MySQL environment

## Includes

- MySQL Server
- [phpMyAdmin](https://www.phpmyadmin.net) MySQL Browser and administration

To open phpMyAdmin, visit [here](http://localhost:8080).

## Starting the environment

```
$ docker compose up
```

## Creating a database from a `.sql` file

1. Execute the line below to open an interactive MySQL shell, make sure to replace `xxx.xxx.x.xxx` with your IP

```
$ docker run -it -v $PWD/db.sql:/tmp/db.sql --rm mysql mysql -h"xxx.xxx.x.xxx" -uroot -pmypassword
```

2. In the interactive shell, type:

```
mysql> source /tmp/db.sql
```